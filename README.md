# eks-helm-pipe

This Bitbucket pipe handles publishing Helm charts to S3 helm repos and installing them on EKS clusters

## Credentials

AWS credentials will be required for interacting with S3 helm repos and installing charts to EKS clusters.

The following environment variables are supported:

```text
AWS_ACCESS_KEY_ID      AWS access key variable
AWS_SECRET_ACCESS_KEY  AWS secret variable
ROLE_ARN               If provided, assume the given IAM role and use the temporary credentials when executing the pipe
```

## Commands

This pipe executes helm commands specified by the variable `COMMAND`.

The following commands are supported:

### lint

Runs `helm lint` against a given chart.

NOTE: the `publish` command executes the `lint` command before publishing the chart.


#### lint variables

```text
CHART  Path to the directory containing the Chart.yaml
       Default: charts/${BITBUCKET_REPO_SLUG}
```

#### lint example

```yaml
- pipe: thirdspacelearningdevs/eks-helm-pipe:0.3.0
  variables:
    COMMAND: lint
    CHART: charts/mychart
```

### publish

Publish packages a chart then pushes it to an S3 Helm repo.

It prevents overwriting existing charts. If a chart has been changed, the chart version should be bumped.

#### publish variables

```text
CHART          Path to the directory containing the Chart.yaml
               Default: charts/${BITBUCKET_REPO_SLUG}

CHART_REPO     The URL of the repo to push the chart to in the format s3://my-chart-repo

FORCE_PUBLISH  Overwrite the chart if it already exists in the repo
               Generally this should be avoided and the chart version should be bumped instead
```

#### publish example

```yaml
- pipe: thirdspacelearningdevs/eks-helm-pipe:0.3.0
  variables:
    COMMAND: publish
    CHART: charts/mychart
    CHART_REPO: s3://my-chart-repo
```

### install

Install a helm chart to an EKS cluster

#### install variables

```text
CHART          Name of the chart including the repo name e.g. stable/metrics-server

CHART_REPO     The URL of the chart repo
               Default: https://kubernetes-charts.storage.googleapis.com

CHART_VERSION  The version of the helm chart to install
               Default: defaults to the latest version of the chart found in $CHART_REPO

CLUSTER_NAME   The name of an EKS cluster in your AWS account
               Default: $BITBUCKET_DEPLOYMENT_ENVIRONMENT

NAMESPACE      The Kubernetes namespace in which the chart should be installed
               Default: ${BITBUCKET_DEPLOYMENT_ENVIRONMENT:-default}}

RELEASE        The helm install release name
               Default: $(basename "$CHART")

IMAGE_TAG      If provided, image.tag will be set to this value when installing the chart

VALUES         Path to helm chart values file to use with install
```

#### install example

```yaml
- pipe: thirdspacelearningdevs/eks-helm-pipe:0.3.0
  variables:
    COMMAND: install
    CHART: stable/metrics-server
    CLUSTER_NAME: my-eks-cluster
```

## Complete Example

Below shows an example of how this pipe could be used in a Bitbucket pipeline

```yaml
pipelines:
  branches:
    master:
      - step:
          name: Publish Chart
          script:
            - pipe: thirdspacelearningdevs/eks-helm-pipe:0.3.0
              variables:
                COMMAND: publish
                CHART: charts/my-amazing-chart
                CHART_REPO: s3://my-chart-repo
      - step:
          name: Install Private Chart
          script:
            - pipe: thirdspacelearningdevs/eks-helm-pipe:0.3.0
              variables:
                COMMAND: install
                CHART: charts/my-amazing-chart
                CHART_REPO: s3://my-chart-repo
                CLUSTER_NAME: my-eks-cluster
                VALUES: values/dev.yaml
                IMAGE_TAG: 1.0.0-beta
      - step:
          name: Install Public Chart
          script:
            - pipe: thirdspacelearningdevs/eks-helm-pipe:0.3.0
              variables:
                COMMAND: install
                CHART: stable/metrics-server
                CHART_VERSION: 2.11.0
                CLUSTER_NAME: my-eks-cluster
```

## Building Docker image

`docker buildx build --platform linux/amd64,linux/arm64 -t $(yq <pipe.yml -r .image) . --push`
