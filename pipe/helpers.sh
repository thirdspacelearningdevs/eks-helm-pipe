#!/usr/bin/env bash

# shellcheck source=pipe/common.sh
source "$(dirname "$0")/common.sh"

lint() {
  info "Linting chart..."
  run helm dependency build "$CHART"
  run helm lint "$CHART"
  [[ $status != "0" ]] && fail "Chart linting failed"
  success "Chart linted successfully"
}

assume_role() {
  info "Assuming AWS IAM Role: $ROLE_ARN"
  session_name="$(echo -n "bitbucket-${BITBUCKET_REPO_FULL_NAME}-${BITBUCKET_BUILD_NUMBER}" | tr '[:upper:]' '[:lower:]' | tr -C '[:alnum:]' '-')"
  info "Role Session Name: $session_name"
  set +e
  json="$(aws sts assume-role --role-arn "$ROLE_ARN" --role-session-name "$session_name")"
  status=$?
  set -e
  [[ $status != "0" ]] && fail "Failed to assume IAM Role: $ROLE_ARN"
  AWS_ACCESS_KEY_ID="$(jq -r .Credentials.AccessKeyId <<< "$json")"
  AWS_SECRET_ACCESS_KEY="$(jq -r .Credentials.SecretAccessKey <<< "$json")"
  AWS_SESSION_TOKEN="$(jq -r .Credentials.SessionToken <<< "$json")"
  export AWS_ACCESS_KEY_ID
  export AWS_SECRET_ACCESS_KEY
  export AWS_SESSION_TOKEN
  success "Successfully assumed role: $ROLE_ARN"
}

ecr_login() {
  aws ecr get-login-password \
     --region eu-west-1 | helm registry login \
     --username AWS \
     --password-stdin 441515337278.dkr.ecr.eu-west-1.amazonaws.com
  status=$?
  [[ $status != "0" ]] && fail "Failed to login to ECR"
  success "Successfully logged into ECR"
}

get_kubeconfig() {
  info "Getting kubeconfig for cluster: $CLUSTER_NAME"
  run aws eks update-kubeconfig --name "$CLUSTER_NAME" --alias cluster
  [[ $status != "0" ]] && fail "Failed getting kubeconfig for cluster: $CLUSTER_NAME"
  success "Successfully got kubeconfig for cluster: $CLUSTER_NAME"
}

publish() {
  info "Packaging helm chart: $CHART"
  run helm dependency build "$CHART"
  run helm package "$CHART"
  [[ $status != "0" ]] && fail "Failed to package chart"
  package="${CHART_NAME}-${CHART_VERSION}.tgz"
  info "Publishing $CHART as $package to ECR"
  CMD="helm push ${package} ${ECR_REPO}"
  run $CMD
  [[ $status != "0" ]] && fail "Failed to publish chart to ECR"
  success "Successfully published chart to ECR"
  success "Name    : $CHART_NAME"
  success "Version : $CHART_VERSION"
  success "ECR     : ${ECR_REPO}/${CHART_NAME}"
}

install() {
  info "Installing helm chart ${CHART} (${CHART_VERSION}) to cluster ${CLUSTER_NAME}"
  get_kubeconfig
  CMD="helm upgrade -i ${RELEASE} ${ECR_REPO}/${CHART_NAME} --atomic --cleanup-on-fail --kube-context cluster --namespace ${NAMESPACE} --version ${CHART_VERSION}"
  [[ -n $VALUES ]] && CMD+=" --values ${VALUES}"
  [[ -n $IMAGE_TAG ]] && CMD+=" --set image.tag=${IMAGE_TAG}"
  run $CMD
  [[ $status != "0" ]] && fail "Failed to install chart $CHART_NAME ($CHART_VERSION) to cluster ${CLUSTER_NAME}"
  success "Successfully installed chart ${CHART_NAME} (${CHART_VERSION}) to cluster ${CLUSTER_NAME}"
}
