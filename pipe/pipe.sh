#!/usr/bin/env bash
#
# This pipe handles publishing Helm charts to ECR based helm repos and installing them on EKS clusters
#

# Source some helper functions
# shellcheck source=pipe/helpers.sh
source "$(dirname "$0")/helpers.sh"

info "Executing eks-helm-pipe..."

# Required parameters
COMMAND=${COMMAND:?'variable required but not set'}
CHART=${CHART:=charts/${BITBUCKET_REPO_SLUG}}
ECR_REPO=${ECR_REPO:=oci://441515337278.dkr.ecr.eu-west-1.amazonaws.com}

case $COMMAND in
  lint) ;; # Not running here, but is a valid command
  publish)
    # Ensure CHART_VERSION is always set from the Chart.yaml
    [[ -n $CHART_VERSION ]] && fail "CHART_VERSION must not be set when publishing a chart as the version should always be taken from the Chart.yaml"
    ;;
  install)
    # Install parameters
    CLUSTER_NAME="${CLUSTER_NAME:=${BITBUCKET_DEPLOYMENT_ENVIRONMENT}}"
    CLUSTER_NAME="${CLUSTER_NAME:?'variable required but not set'}"
    NAMESPACE="${NAMESPACE:=${BITBUCKET_DEPLOYMENT_ENVIRONMENT:-default}}"
    RELEASE="${RELEASE:=$(basename "$CHART")}"
    IMAGE_TAG="${IMAGE_TAG:=""}"
    ;;
  *) fail "$COMMAND not valid for variable COMMAND" ;;
esac

# AWS parameters (optional)
export AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID:=""}
export AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:=""}
export AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:="eu-west-1"}
ROLE_ARN=${ROLE_ARN:=""}

# Other parameters
DEBUG=${DEBUG:="false"}
CHART_VERSION="${CHART_VERSION:=""}"

# Short Names
CHART_NAME="$(basename "$CHART")"

# Login to ECR before assuming the deployer role

info "Logging into ECR"
ecr_login
[[ $status != "0" ]] && fail "Failed to login to ECR"

# Assume an AWS IAM role if a role ARN was provided
[[ -n $ROLE_ARN ]] && assume_role

# Lookup the chart version if not set
CHART_VERSION=${CHART_VERSION:=$(helm show chart "$CHART" | grep '^version:' | cut -d ' ' -f2)}

# Execute the required command
case $COMMAND in
  lint) lint ;;
  publish) lint && publish ;;
  install) install ;;
esac
